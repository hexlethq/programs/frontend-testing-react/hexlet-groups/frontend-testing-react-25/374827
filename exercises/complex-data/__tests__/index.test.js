const faker = require('faker');

// BEGIN
const getTransaction = () => faker.helpers.createTransaction();
test.each([
    'amount',
    'date',
    'business',
    'name',
    'type',
    'account',
])('createTransaction contain (%s)', (a) => {
    expect(getTransaction()).toHaveProperty(a);
});
test('each call to createTransaction() returns an object with different data', () => {
    expect(getTransaction()).not.toEqual(getTransaction());
});
// END
