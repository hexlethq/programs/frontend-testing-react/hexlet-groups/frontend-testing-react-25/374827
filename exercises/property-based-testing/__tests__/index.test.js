const fc = require('fast-check');

const sort = (data) => data.slice().sort((a, b) => a - b);

// BEGIN
test('sort arr', () => {
    fc.assert(
        fc.property(fc.array(fc.integer()), (data) => {
            expect(sort(data)).toBeSorted();
        })
    );
});
// END
