test('main', () => {
    const src = {k: 'v', b: 'b'};
    const target = {k: 'v2', a: 'a'};
    const result = Object.assign(target, src);

    expect.hasAssertions();

    // BEGIN
    expect(result).toEqual(target);
    expect(result).toHaveProperty('k', 'v');
    expect(result).toHaveProperty('b', 'b');
    expect(result).toHaveProperty('a', 'a');
    // END
});
