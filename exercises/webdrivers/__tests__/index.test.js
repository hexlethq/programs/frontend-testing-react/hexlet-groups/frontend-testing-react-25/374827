const puppeteer = require('puppeteer');
const getApp = require('../server/index.js');

const port = 5001;
const appUrl = `http://localhost:${port}`;
const appArticlesUrl = `http://localhost:${port}/articles`;
const appNewArticleUrl = `http://localhost:${port}/articles/new`;

let browser;
let page;

const app = getApp();

describe('simple blog works', () => {
  beforeAll(async () => {
    await app.listen(port, '0.0.0.0');
    browser = await puppeteer.launch({
      args: ['--no-sandbox', '--disable-gpu'],
      headless: true,
      // slowMo: 250
    });
    page = await browser.newPage();
    await page.setViewport({
      width: 1280,
      height: 720,
    });
  });

  // BEGIN
  test('main page open', async () => {
    await page.goto(appUrl);
    await expect(page.title()).resolves.toBe('Simple Blog')
    const isWelcomeDivExist = await page.$eval('#title', el => !!el)
    expect(isWelcomeDivExist).toBeTruthy();
  })
  test('article list exist', async () => {
    await page.goto(appArticlesUrl);
    await page.waitForSelector("#articles");
    const isArticlesExist = await page.$eval('#articles', el => !!el)
    expect(isArticlesExist).toBeTruthy();
  })
  test('form "new article" exist', async () => {
    await page.goto(appArticlesUrl);
    await page.click('[href="/articles/new"]');
    const isFormExist = await page.$eval('form', el => !!el)
    expect(isFormExist).toBeTruthy();
  })
  test("complete \"new article\" the form", async () => {
    await page.goto(appNewArticleUrl);
    await page.waitForSelector("form");
    await page.click("#name");
    await page.type("#name", 'Test');
    await page.select("#category", '1');
    await page.click("#content");
    await page.type("#content", 'content');
    await page.click("input[type=submit]");
    await page.waitForSelector("#articles");
    const bodyContent = await page.$eval('html body', el => el.innerHTML)
    expect(bodyContent).toContain('Test');
  })
  test('edit article', async () => {
    await page.goto(appArticlesUrl);
    await page.click('#articles tr:last-child td:last-child a')
    const url = await page.url()
    expect(url).toMatch('edit')
    await page.waitForSelector("form");
    await page.click("#name");
    await page.type("#name", 'edited');
    await page.click("input[type=submit]");
    await page.waitForSelector("#articles");
    const bodyContent = await page.$eval('html body', el => el.innerHTML)
    expect(bodyContent).toContain('edited');
  }, 20000)
  // END

  afterAll(async () => {
    // delete test item
    await page.goto(appArticlesUrl);
    await page.waitForSelector("#articles");
    await page.click('#articles tr:last-child td:last-child form input[type=submit]');

    await browser.close();
    await app.close();
  });
});
