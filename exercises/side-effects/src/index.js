const fs = require('fs');

// BEGIN
const upVersion = (path, updateVersionType = 'patch') => {
    const version = fs.readFileSync(path, "utf8")
    const versionArr = JSON.parse(version)?.version.split('.').map(Number)
    const versionTypeMap = {
        major: 0,
        minor: 1,
    }
    const index = versionTypeMap[updateVersionType] ?? 2
    versionArr[index] = versionArr[index] + 1
    versionArr.fill(0, index + 1)
    fs.writeFileSync(path, JSON.stringify({version: versionArr.join('.')}))
}
// END
module.exports = {upVersion};
