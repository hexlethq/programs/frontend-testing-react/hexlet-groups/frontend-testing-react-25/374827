const fs = require('fs');
const path = require('path');
const { upVersion } = require('../src/index.js');

// BEGIN
const INITIAL_DATA = { version: '1.3.2' }
const FIXTURE_PATH = path.join(__dirname, '..', '__fixtures__', 'package.json');
const getVersion = () => {
    const strVersion = fs.readFileSync(FIXTURE_PATH, 'utf-8');
    return JSON.parse(strVersion)?.version
}
const resetInitialData = () => fs.writeFileSync(FIXTURE_PATH, JSON.stringify(INITIAL_DATA), 'utf-8');

describe('up version', () => {
    beforeEach(() => {
        resetInitialData();
    });
    test.each([
        ['major', '2.0.0'],
        ['minor', '1.4.0'],
        ['patch', '1.3.3'],
    ])('%s', (typeVersion, expected) => {
        upVersion(FIXTURE_PATH, typeVersion);
        expect(getVersion()).toBe(expected);
    });
    test('without second arg', () => {
        upVersion(FIXTURE_PATH);
        expect(getVersion()).toBe('1.3.3');
    })
    afterAll(() => {
        resetInitialData();
    })
})
// END
