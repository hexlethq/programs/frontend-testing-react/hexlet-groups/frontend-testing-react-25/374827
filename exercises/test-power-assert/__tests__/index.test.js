const assert = require('power-assert');
const { indexOf } = require('lodash');

// BEGIN
assert(indexOf([1, 2, 1, 2], 2) === 1);
assert(indexOf([1, 2, 1, 2], 2, 2) === 3);
assert(indexOf([2, 'one', 'cat', false], 8) === -1);
// END

// ПЕРЕД ТЕМ КАК НАЧАТЬ РЕШАТЬ ЗАДАНИЕ
// УДАЛИТЕ СЛЕДУЮЩУЮ СТРОКУ
