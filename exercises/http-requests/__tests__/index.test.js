const nock = require('nock');
const axios = require('axios');
const { get, post } = require('../src/index.js');

axios.defaults.adapter = require('axios/lib/adapters/http');
// nock.disableNetConnect();
// BEGIN
test('test get', async () => {
    nock(/example\.com/)
        .get('/users')
        .reply(200, []);
    const mainLanguage = await get('https://example.com/users');
    expect(mainLanguage?.data).toEqual([]);
})

test('test post', async () => {
    nock(/example\.com/)
        .post('/users')
        .reply(200, 'Fedor added');
    const mainLanguage = await post('https://example.com/users', {
        firstname: 'Fedor',
        lastname: 'Sumkin',
        age: 33
    });
    expect(mainLanguage?.data).toEqual('Fedor added');
})
// END
