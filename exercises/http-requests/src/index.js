const axios = require('axios');

// BEGIN

const get = async (url) => {
    try {
        return await axios.get(url);
    } catch (e) {
        console.error(e);
    }
}

const post = async (url, body) => {
    try {
        return await axios.post(url, body);
    } catch (e) {
        console.error(e);
    }
}

// END

module.exports = { get, post };
